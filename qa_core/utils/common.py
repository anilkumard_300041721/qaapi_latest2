import logging
import sys
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
from os.path import basename
from email import encoders


def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter("%(message)s")
    streamhandler = logging.StreamHandler(sys.stdout)
    streamhandler.setFormatter(formatter)
    filehandler = logging.FileHandler('{}.log'.format(logger_name))
    filehandler.setFormatter(formatter)

    logger.setLevel(logging.DEBUG)
    logger.addHandler(streamhandler)
    logger.addHandler(filehandler)

    return logger


def send_mail(send_to, subject, body, filename, attachment_name='', send_from='myntra.datasciences@gmail.com', server='smtp.gmail.com', port='587',
              username='myntra.datasciences@gmail.com', password='nqwihqliixmhfncc'):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))

    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(filename, "rb").read())
    encoders.encode_base64(part)

    part.add_header("Content-Disposition", "attachment", filename=attachment_name)
    msg.attach(part)
    smtp = smtplib.SMTP(server, port)
    smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()