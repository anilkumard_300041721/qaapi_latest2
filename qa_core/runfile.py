from flask import Flask, jsonify
from flask import request
from flask import abort
from qa_function import get_qa_record
import time
import json
import os

app = Flask(__name__)
app.config['JSON_SORT_KEYS']=False


@app.route('/sentient/qa/api/v1.0', methods=['POST'])
def get_record():
    try:
        record = get_qa_record(request.json)
        print(record)
        if record is None:
            abort(400)
        return jsonify(record)
    except Exception:
        abort(400)


if __name__ == '__main__':
    app.run(debug=True, port=2777, host='0.0.0.0')

    # with open('sample.json', 'r') as k:
    #     j = json.load(k)
    # print(j)
    # record = get_qa_record(j)
    # print(record)
    # changes




