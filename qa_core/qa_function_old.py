# coding: utf-8

from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from nltk.corpus import stopwords
import string
import re
import spacy
import datetime
import calendar
import fileinput
import ast, json

import pandas as pd
import fileinput



# coding: utf-8
import joblib
from nlp_utils import *
from qa_config import *
import html2text

# A custom stoplist
STOPLIST = set(stopwords.words('english') + list(ENGLISH_STOP_WORDS))
# List of symbols we don't care about
SYMBOLS = []
parser = spacy.load('en')

# path to load token objects from qa google spreadsheet
token_obj_path = token_obj_path

# loading content objects from qa google spreadsheet
ack_tokens = joblib.load(token_obj_path + 'ack_tokens.pkl')
apology_tokens = joblib.load(token_obj_path + 'apology_tokens.pkl')
reassure_tokens = joblib.load(token_obj_path + 'reassure_tokens.pkl')
greetings_tokens = joblib.load(token_obj_path + 'greetings_tokens.pkl')
eng_cuss_words = joblib.load(token_obj_path + 'eng_cuss_words.pkl')
ack_ngram_hing = joblib.load(token_obj_path + 'ack_ngram_hing.pkl')
apology_ngram_hing = joblib.load(token_obj_path + 'apology_ngram_hing.pkl')
reassure_ngram_hing = joblib.load(token_obj_path + 'reassure_ngram_hing.pkl')
zero_tolerance_ngram_hing = joblib.load(token_obj_path + 'zero_tolerance_ngram_hing.pkl')
closing_tokens = joblib.load(token_obj_path + 'closing_tokens.pkl')
closing_ngram_hing = joblib.load(token_obj_path + 'closing_ngram_hing.pkl')
hold_time_thresh = int(joblib.load(token_obj_path + 'hold_time.pkl')[0][0])
talk_time_thresh = int(joblib.load(token_obj_path + 'talk_time.pkl')[0][0])
dead_air_thresh = int(joblib.load(token_obj_path + 'dead_air.pkl')[0][0])
email_weights = joblib.load(token_obj_path + 'email_weights.pkl')
call_weights = joblib.load(token_obj_path + 'call_weights.pkl')


# In[130]:

def qa_score(text, hold_time, talk_time, dead_air, email=1, extra_params=False):
    text_tokens = tokenizeText(text)

    score_real = {'ack_score': 0,
                  'apology_score': 0,
                  'reassure_score': 0,
                  'greetings_score': 0,
                  'zero_tolerance_score': 0,
                  'closing_score': 0}

    for i in text_tokens:
        if i in ack_tokens:
            score_real['ack_score'] += 1
        if i in apology_tokens:
            score_real['apology_score'] += 1
        if i in reassure_tokens:
            score_real['reassure_score'] += 1
        if i in greetings_tokens:
            score_real['greetings_score'] += 1
        if i in eng_cuss_words:
            score_real['zero_tolerance_score'] += 1
        if i in closing_tokens:
            score_real['closing_score'] += 1

    text_ngrams = ngrams(text, 3)

    for i in text_ngrams:
        if i in ack_ngram_hing:
            score_real['ack_score'] += 1
        if i in apology_ngram_hing:
            score_real['apology_score'] += 1
        if i in reassure_ngram_hing:
            score_real['reassure_score'] += 1
        if i in zero_tolerance_ngram_hing:
            score_real['zero_tolerance_score'] += 1
        if i in closing_ngram_hing:
            score_real['closing_score'] += 1

    # converting score to binary flag
    score_binary = score_real.copy()
    for key in score_binary.keys():
        if key == 'zero_tolerance_score':
            if score_binary[key] > 0:
                score_binary[key] = 1
        else:
            if score_binary[key] == 0:
                score_binary[key] = 1
            else:
                score_binary[key] = 0

    # checking for on-call performance parameters
    perf_para = {'hold_time': 0,
                 'talk_time': 0,
                 'dead_air': 0}

    if hold_time >= hold_time_thresh:
        perf_para['hold_time'] = 1
    if talk_time >= talk_time_thresh:
        perf_para['talk_time'] = 1
    if dead_air >= dead_air_thresh:
        perf_para['dead_air'] = 1

    # inverting values of score binary and perf para dicts to calculate weighted score
    score_binary_inv = {key: int(not (score_binary[key])) for key in score_binary}
    perf_para_inv = {key: int(not (perf_para[key])) for key in perf_para}

    if extra_params is False:
        perf_para['hold_time'] = 1
        perf_para['talk_time'] = 1
        perf_para['dead_air'] = 1

    # 1 being used as place holder for resolution score - to be incorporated
    if email == 1:
        # email score calculation
        score = 0
        score = score_binary_inv['ack_score'] * email_weights['Acknowledgement'] + score_binary_inv['apology_score'] * \
                email_weights['Apology'] + score_binary_inv['reassure_score'] * email_weights['Reassurance'] + \
                score_binary_inv['zero_tolerance_score'] * email_weights['Zero Tolerance'] + score_binary_inv[
                    'greetings_score'] * email_weights['Greetings'] + score_binary_inv['closing_score'] * email_weights[
                    'Closing'] + 1 * email_weights['Resolution']
    elif email == 0:
        # call score calculation
        score = 0
        score = score_binary_inv['ack_score'] * call_weights['Acknowledgement'] + score_binary_inv['apology_score'] * \
                call_weights['Apology'] + score_binary_inv['reassure_score'] * call_weights['Reassurance'] + \
                score_binary_inv['zero_tolerance_score'] * call_weights['Zero Tolerance'] + score_binary_inv[
                    'greetings_score'] * call_weights['Greetings'] + score_binary_inv['closing_score'] * call_weights[
                    'Closing'] + 1 * email_weights['Resolution'] + perf_para_inv['hold_time'] * call_weights[
                    'Hold Time'] + perf_para_inv['talk_time'] * call_weights['Talk Time'] + perf_para_inv['dead_air'] * \
                call_weights['Dead Air']

    # returning score
    return score_binary, score_real, perf_para, score


def get_empty_record():

    record = {}
    record["incident_id"] = ""
    record["interaction_id"] = ""
    record["emp_name"] = ""
    record["email_content"] = ""
    record["customer_call_content"] = ""
    record["champion_call_content"] = ""
    record["acknowledgement"] = False
    record["apology"] = False
    record["reassurance"] = False
    record["greetings"] = False
    record["zero_tolerance"] = False
    record["flagged"] = False
    record["source"] = ""
    record["hour"] = 0
    record["date"] = 0
    record["week"] = 0
    record["month"] = 0
    record["year"] = 0
    record["interaction_timestamp"] = ""
    return record


def get_qa_record(line):
    message_interaction = json.loads(line)
    content = ""
    record = get_empty_record()
    record["incident_id"] = message_interaction["incident_id"]
    # record["order_id"] = message_interaction["order_id"]
    if "crt_obj_id" in message_interaction:
        # Call
        content = message_interaction["champion_text"]
        record["emp_name"] = message_interaction["champion_name"]
        record["interaction_id"] = message_interaction["crt_obj_id"]
        record["customer_call_content"] = message_interaction["customer_text"]
        record["champion_call_content"] = message_interaction["champion_text"]
        record["source"] = "Call"
        record["interaction_timestamp"] = message_interaction["timestamp"]
        record["hold_time_val"] = int(message_interaction.get("hold_time", 0))
        record["talk_time_val"] = int(message_interaction.get("talk_time", 0))
        record["dead_air_val"] = int(float(message_interaction.get("dead_air", 0)))
        record["merged_text"] = message_interaction.get("merged_text", "")
        record["phone"] = message_interaction.get("phone", "")
    else:
        # Email
        text_maker = html2text.HTML2Text()
        text_maker.ignore_links = True
        text_maker.bypass_tables = False
        content = text_maker.handle(message_interaction["email_content"])
        record["interaction_id"] = message_interaction["incident_thread_id"]
        record["emp_name"] = message_interaction["author"]
        record["email_content"] = content
        record["source"] = "Mail"
        record["interaction_timestamp"] = message_interaction["timestamp"].strip('\'')
        record["hold_time_val"] = int(message_interaction.get("hold_time", 0))
        record["talk_time_val"] = int(message_interaction.get("talk_time", 0))
        record["dead_air_val"] = int(float(message_interaction.get("dead_air", 0)))
        record["champion_id"] = message_interaction.get("champion_id", "")

    record["user_email"] = record.get("user_email", "")
    score_binary, score_real, perf_para, score = qa_score(content, record["hold_time_val"], record["talk_time_val"],
                                                          record["dead_air_val"], record["source"] == "Mail", True)

    record["acknowledgement"] = bool(score_binary["ack_score"])
    record["apology"] = bool(score_binary["apology_score"])
    record["reassurance"] = bool(score_binary["reassure_score"])
    record["greetings"] = bool(score_binary["greetings_score"])
    record["zero_tolerance"] = bool(score_binary["zero_tolerance_score"])
    record["closing"] = bool(score_binary["closing_score"])

    # record["acknowledgement_val"] = bool(score_real["ack_score"])
    # record["apology_val"] = bool(score_real["apology_score"])
    # record["reassurance_val"] = bool(score_real["reassure_score"])
    # record["greetings_val"] = bool(score_real["greetings_score"])
    # record["zero_tolerance_val"] = bool(score_real["zero_tolerance_score"])

    record["hold_time"] = bool(score_real.get("hold_time", 0))
    record["talk_time"] = bool(score_real.get("talk_time", 0))
    record["dead_air"] = bool(score_real.get("dead_air", 0))

    # removing these keys, as entire record dumped to memsql
    record.pop("hold_time_val", None)
    record.pop("talk_time_val", None)
    record.pop("dead_air_val", None)

    record["score"] = int(score)

    record["flagged"] = record["acknowledgement"] or record["apology"] or record["reassurance"] or record[
        "greetings"] or record["zero_tolerance"]
    interaction_timestamp_dt = datetime.datetime.strptime(record["interaction_timestamp"], '%Y-%m-%d %H:%M:%S')
    record["hour"] = interaction_timestamp_dt.hour
    record["date"] = int(interaction_timestamp_dt.strftime("%Y%m%d"))
    record["week"] = interaction_timestamp_dt.isocalendar()[1]
    record["month"] = calendar.month_name[interaction_timestamp_dt.month].upper()
    record["year"] = interaction_timestamp_dt.year

    record["predicted_dispostion_l1"] = message_interaction.get("predicted_dispostion_l1", "")
    record["predicted_dispostion_l2"] = message_interaction.get("predicted_dispostion_l2", "")
    record["predicted_dispostion_l3"] = message_interaction.get("predicted_dispostion_l3", "")

    record["actual_dispostion_l1"] = message_interaction.get("actual_dispostion_l1", "")
    record["actual_dispostion_l2"] = message_interaction.get("actual_dispostion_l2", "")
    record["actual_dispostion_l3"] = message_interaction.get("actual_dispostion_l3", "")

    if record["source"] == "Mail" and record["emp_name"] == "Customer":
        m = 1
    else:
        return record
    return None
line3 = json.loads('input_sample.txt')
print(get_qa_record(line3))
