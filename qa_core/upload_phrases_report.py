from pandas import ExcelWriter
from datetime import datetime, timedelta
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from qa_config import service_account_secret_path
import pickle

def callback(request_id, response, exception):
    if exception:
        # Handle error
        print(exception)
    else:
        print("Permission Id: %s" % response.get('id'))
def upload_file_to_service_account(file_name, email):
    scopes = ['https://www.googleapis.com/auth/drive']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(service_account_secret_path, scopes)

    print("Authorising")
    http_auth = credentials.authorize(Http())
    drive = build('drive', 'v3', http=http_auth)

    print("Uploading files")

    mime_type = "application/vnd.ms-excel"

    media = MediaFileUpload(file_name,mimetype=mime_type,resumable=True)
    file_p = drive.files().create(body={'name': file_name, 'parents':['14WcFrlqG4gZop-apkLBX1_PEb3KzJotW']},media_body=media,).execute()
    drive.files().create()

    print('File ID: %s' % file_p.get('id'))

    file_id = file_p.get('id')

    batch = drive.new_batch_http_request(callback=callback)
    user_permission = {'type': 'user','role': 'writer','emailAddress': email}
    batch.add(drive.permissions().create(fileId=file_id,body=user_permission,fields='id',))
    batch.execute()

def uploadAndResetPhrasesReport(phrasesReportDict):
    dataFramesList = []
    sheetsNames = []
    for parameter, parameterDict in phrasesReportDict.items():
        df = pd.DataFrame(data=parameterDict, index=[0])
        df = (df.T)
        dataFramesList.append(df)
        sheetsNames.append('last16k_' + parameter)

    resetCumulative = 0
    if resetCumulative == 0:
        with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'rb') as cumulativePhrasesReportDictF:
            cumulativePhrasesReportDict = pickle.load(cumulativePhrasesReportDictF)

        for param, paramDict in cumulativePhrasesReportDict.items():
            if param != 'statistics':
                for keyPhrase, freqCount in paramDict.items():
                    cumulativePhrasesReportDict[param][keyPhrase] += phrasesReportDict[param][keyPhrase]
            else:
                for keyPhrase, freqCount in paramDict.items():
                    if keyPhrase != 'start_time':
                        cumulativePhrasesReportDict[param][keyPhrase] += phrasesReportDict[param][keyPhrase]

        with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'wb') as f:
            pickle.dump(cumulativePhrasesReportDict, f, pickle.HIGHEST_PROTOCOL)

    phrasesReportDict2 = {}
    phrasesReportDict2['statistics'] = {'no_of_calls': 0, 'repetition': 0, 'over_apology': 0, 'favourable': 0,
                                       'unfavourable': 0, 'opening': 0, 'closing': 0, 'ztp': 0, 'hold_time': 0, 'ZTP_CustomerCheck':0, 'nofCouponCalls':0, 'CouponConditions':0, 'Jargons':0, 'champ_voice_not_audible':0,'start_time': str(datetime.now())}
    with open('../pickle_files/champ_check_param_ident_dict.pkl', 'rb') as champ_check_param_ident_dict_file:
        champ_check_param_ident_dict = pickle.load(champ_check_param_ident_dict_file)

    with open('../pickle_files/cust_check_param_ident_dict.pkl', 'rb') as cust_check_param_ident_dict_file:
        cust_check_param_ident_dict = pickle.load(cust_check_param_ident_dict_file)

    for keyPhrase, param in champ_check_param_ident_dict.items():
        if param not in phrasesReportDict2:
            phrasesReportDict2[param] = {}
        if keyPhrase not in phrasesReportDict2[param]:
            phrasesReportDict2[param][keyPhrase] = 0

    for keyPhrase, param in cust_check_param_ident_dict.items():
        if param not in phrasesReportDict2:
            phrasesReportDict2[param] = {}
        if keyPhrase not in phrasesReportDict2[param]:
            phrasesReportDict2[param][keyPhrase] = 0
    with open('../pickle_files/phrasesReport.pkl', 'wb') as f:
        pickle.dump(phrasesReportDict2, f, pickle.HIGHEST_PROTOCOL)

    if resetCumulative == 1:
        cumulativePhrasesReportDict = phrasesReportDict
        with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'wb') as f:
            pickle.dump(cumulativePhrasesReportDict, f, pickle.HIGHEST_PROTOCOL)

    with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'rb') as cumulativePhrasesReportDictF:
        cumulativePhrasesReportDict = pickle.load(cumulativePhrasesReportDictF)

    for parameter, parameterDict in cumulativePhrasesReportDict.items():
        df = pd.DataFrame(data=parameterDict, index=[0])
        df = (df.T)
        dataFramesList.append(df)
        sheetNameL = 'cumulative_' + parameter
        if sheetNameL == 'cumulative_champ_voice_not_audible':
            sheetNameL = 'cumul_champ_not_audible'
        sheetsNames.append(sheetNameL)

    outputFilePath = '../phrases_report/Phrases_Report_' + str(datetime.now().date()) + '.xlsx'
    writer = ExcelWriter(outputFilePath, engine='xlsxwriter')
    for n, df in enumerate(dataFramesList):
        df.to_excel(writer, sheet_name=sheetsNames[n])
    writer.save()
    print("Uploading the phrases report ... ")
    upload_file_to_service_account(outputFilePath, email=["sagar.grover@myntra.com"])