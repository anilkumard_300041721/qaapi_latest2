import json
import datetime
def getDeadAir(custPerWordInfo,champPerWordInfo):
    max_dead_air = 0
    deadAirTimeStamp = 0
    for custWord in custPerWordInfo:
        custWord.append(0) # 0 - customer
    for champWord in champPerWordInfo:
        champWord.append(1) #1-champion
    combinedPerWordInfo = custPerWordInfo + champPerWordInfo
    combinedPerWordInfo = sorted(combinedPerWordInfo, key=lambda x: float(x[1]), reverse=False)
    nofWords = len(combinedPerWordInfo)
    for wordIndex in range(nofWords):
        if combinedPerWordInfo[wordIndex][6] == 0:
            if wordIndex+1 < nofWords:
                if combinedPerWordInfo[wordIndex+1][6] == 1:
                    timeGap = combinedPerWordInfo[wordIndex+1][1] - combinedPerWordInfo[wordIndex][2]
                    if timeGap > max_dead_air:
                        max_dead_air = timeGap
                        deadAirTimeStamp = combinedPerWordInfo[wordIndex][2]
                        #print(max_dead_air,deadAirTimeStamp)
    return max_dead_air,deadAirTimeStamp,combinedPerWordInfo

def getTimeDependantParams(customer_text,champion_text,per_word_info_cust,per_word_info_champ,merged_info):
    custPerWordInfo = per_word_info_cust
    custTextSp = customer_text.split()
    champPerWordInfo = per_word_info_champ
    champTextSp = champion_text.split()
    mergedText = merged_info
    custTextSpLen = len(custTextSp)
    champTextSpLen = len(champTextSp)
    custSentInfoFromInput = []
    champSentInfoFromInput = []
    champPerWordInfoModified = []
    custPerWordInfoModified = []
    if len(custTextSp) != len(per_word_info_cust) or len(champTextSp) != len(per_word_info_champ):
        print(len(custTextSp),len(per_word_info_cust),len(champTextSp),len(per_word_info_champ))
        print("Nof words not matching in word info and text")
        return None,None,None,None,None
    for idx,word in enumerate(champTextSp):
        champPerWordInfoModified.append([word,champPerWordInfo[idx][1],champPerWordInfo[idx][2],1])
    for idx,word in enumerate(custTextSp):
        custPerWordInfoModified.append([word,custPerWordInfo[idx][1],custPerWordInfo[idx][2],0])
    combinedPerWordInfoWithWords = champPerWordInfoModified + custPerWordInfoModified
    combinedPerWordInfoWithWords = sorted(combinedPerWordInfoWithWords, key=lambda x: float(x[1]), reverse=False)

    # max dead air computation
    maxDeadAir,deadAirTimeStamp,sortedCombinedPerWordInfo = getDeadAir(custPerWordInfo,champPerWordInfo)
    #print("---",sortedCombinedPerWordInfo)
    for sentInfo in mergedText:
        if sentInfo[0] == 0: #voice by customer
            #custVoiceTime += ()
            custSentInfoFromInput.append(sentInfo)
        elif sentInfo[0] == 1: #voice by champion
            #nofWordsByChamp += len(sentInfo[1].split())
            champSentInfoFromInput.append(sentInfo)
    curWordPointerInCustSen = 0
    for senInfo1 in custSentInfoFromInput:
        senInfo1.append(custPerWordInfo[curWordPointerInCustSen][1])
        curSenSp = senInfo1[1].split()
        curWordPointerInCustSen += len(curSenSp)-1
        senInfo1.append(custPerWordInfo[curWordPointerInCustSen][2])
        curWordPointerInCustSen += 1
    curWordPointerInChampSen = 0
    for senInfo2 in champSentInfoFromInput:
        senInfo2.append(champPerWordInfo[curWordPointerInChampSen][1])
        curSenSp = senInfo2[1].split()
        curWordPointerInChampSen += len(curSenSp) - 1
        senInfo2.append(champPerWordInfo[curWordPointerInChampSen][2])
        curWordPointerInChampSen += 1

    # Rate of speech computation
    nofWordsByChamp = 0
    nofWordsByCust = 0
    champVoiceTime = 0
    custVoiceTime = 0
    for sentInfo in custSentInfoFromInput:
        nofWordsByCust += len(sentInfo[1].split())
        senDuration = sentInfo[5] - sentInfo[4]
        if senDuration < 0:
            print(sentInfo)
            senDuration = 0
            print("sentence duration is -ve")
        custVoiceTime += senDuration
    for sentInfo in champSentInfoFromInput:
        nofWordsByChamp += len(sentInfo[1].split())
        senDuration = sentInfo[5] - sentInfo[4]
        if senDuration < 0:
            print(sentInfo)
            print("sentence duration is -ve")
            senDuration = 0
        champVoiceTime += senDuration
    #print(nofWordsByCust,nofWordsByChamp,custVoiceTime,champVoiceTime)
    if nofWordsByCust != custTextSpLen or nofWordsByChamp != champTextSpLen:
        print("Nof words not matching ... ")
        return None, None, None, None, None
    if float(custVoiceTime) == 0 or float(champVoiceTime) == 0:
        return None, None, None, None, None
    custROS = (nofWordsByCust/float(custVoiceTime))*60
    champROS = (nofWordsByChamp / float(champVoiceTime)) * 60
    #print(nofWordsByCust,custVoiceTime,nofWordsByChamp,champVoiceTime)

    #print(custSentInfoFromInput)
    lenChampPerWordInfoModified = len(champPerWordInfoModified)
    lenCustSentInfoFromInput = len(custSentInfoFromInput)
    ### combining nearest segments in customer sentence info
    modCustSentInfoFromInput = []
    counterTemp = 0
    #print(custSentInfoFromInput)
    while counterTemp < lenCustSentInfoFromInput-1:
        newlyFormedSegment = [custSentInfoFromInput[counterTemp][1],custSentInfoFromInput[counterTemp][4],custSentInfoFromInput[counterTemp][5]]
        curSegmentEndTime = custSentInfoFromInput[counterTemp][5]
        nextSegmentStartTime = custSentInfoFromInput[counterTemp+1][4]
        while nextSegmentStartTime-curSegmentEndTime < 0.8 and counterTemp < lenCustSentInfoFromInput:
            counterTemp+=1
            if counterTemp < lenCustSentInfoFromInput-1:
                newlyFormedSegment[0] = newlyFormedSegment[0] + " " + custSentInfoFromInput[counterTemp][1]
                newlyFormedSegment[2] = custSentInfoFromInput[counterTemp][5]
                curSegmentEndTime = custSentInfoFromInput[counterTemp][5]
                nextSegmentStartTime = custSentInfoFromInput[counterTemp+1][4]
        modCustSentInfoFromInput.append(newlyFormedSegment)
        counterTemp += 1

    #print("champ last word info :",champPerWordInfoModified[-1])
    #print("---",modCustSentInfoFromInput)
    #print("---",champPerWordInfoModified)
    #print(dfgfgfds)
    breakOuterLoop = 0
    senPointerCust = 0
    wordPointerChamp = 0
    #print(custSentInfoFromInput)
    #print(champPerWordInfoModified)
    Interrupts = []
    curInterrupt = []
    lenModifiedCustSentInfoFromInput = len(modCustSentInfoFromInput)
    while wordPointerChamp < lenChampPerWordInfoModified and senPointerCust < lenModifiedCustSentInfoFromInput:
        #print(wordPointerChamp,senPointerCust)
        curWordInfo = champPerWordInfoModified[wordPointerChamp]
        #print(curWordInfo)
        #print(custSentInfoFromInput[senPointerCust][1])
        while len(modCustSentInfoFromInput[senPointerCust][0].split()) < 3:
            senPointerCust += 1
            if senPointerCust >= lenModifiedCustSentInfoFromInput:
                breakOuterLoop = 1
                break
            #print(wordPointerChamp, senPointerCust)
        if breakOuterLoop == 1:
            continue
        curSenInfo = modCustSentInfoFromInput[senPointerCust]
        curSenSp = curSenInfo[0].split()
        curWordStTime = curWordInfo[1]
        curWordEndTime = curWordInfo[2]
        curSenStTime = curSenInfo[1]
        curSenEndTime = curSenInfo[2]
        uselessWords = ['ok', 'ji', 'okay', 'sir', 'yes', 'haan', 'theek', 'hai', 'ke', 'o']
        if curWordStTime < curSenStTime:
            wordPointerChamp += 1
            continue
        elif curWordStTime >= curSenStTime and curWordStTime <= curSenEndTime:
            if curWordInfo[0] not in uselessWords:
                curInterrupt.append([curWordInfo[0],str(datetime.timedelta(seconds=curWordStTime))[0:10],str(datetime.timedelta(seconds=curWordEndTime))[0:10]])
                prevWordInfo = curWordInfo
            wordPointerChamp += 1
        elif curWordStTime >= curSenEndTime:
            curInterruptCustSentLenWOStopwords = len(curSenSp)
            for wordd in curSenSp:
                if wordd in uselessWords:
                    curInterruptCustSentLenWOStopwords -= 1
            if len(curInterrupt)>0 and curInterruptCustSentLenWOStopwords >= 3:
                #print("###", str(datetime.timedelta(seconds=curWordStTime))[0:10],str(datetime.timedelta(seconds=curWordEndTime))[0:10])
                wordInfo2Pointer = 0
                lenSortedCombinedPerWordInfo = len(sortedCombinedPerWordInfo)
                while wordInfo2Pointer < lenSortedCombinedPerWordInfo:
                    if sortedCombinedPerWordInfo[wordInfo2Pointer][1] == prevWordInfo[1] and sortedCombinedPerWordInfo[wordInfo2Pointer][2] == prevWordInfo[2]:
                        nofChampWordsInNext8 = 0
                        wordInfo2Pointer2 = wordInfo2Pointer
                        while wordInfo2Pointer2 < wordInfo2Pointer+8 and wordInfo2Pointer2 < lenSortedCombinedPerWordInfo:
                            #print(wordInfo2Pointer,wordInfo2Pointer2)
                            if sortedCombinedPerWordInfo[wordInfo2Pointer2][6] == 1:
                                nofChampWordsInNext8 += 1
                            wordInfo2Pointer2 += 1
                        if nofChampWordsInNext8 > 4 or len(curInterrupt) > 4:
                            #print("---",nofChampWordsInNext8,str(datetime.timedelta(seconds=curWordStTime))[0:10],str(datetime.timedelta(seconds=curWordEndTime))[0:10])
                            Interrupts.append({"Cust_sen_Info": [curSenInfo[0],str(datetime.timedelta(seconds=curSenStTime))[0:10],str(datetime.timedelta(seconds=curSenEndTime))[0:10]],"champ_words_info": curInterrupt,"nof_Champ_Words_In_Next8":nofChampWordsInNext8})
                        break
                    wordInfo2Pointer+=1
            curInterrupt = []
            senPointerCust += 1
        else:
            wordPointerChamp += 1
            #print(curWordInfo,curSenInfo)
            continue
    interruptsTimestamps = []
    for interrupt in Interrupts:
        #print(interrupt)
        interruptsTimestamps.append(interrupt['champ_words_info'][0][1])
    return interruptsTimestamps,round(maxDeadAir,2),str(datetime.timedelta(seconds=deadAirTimeStamp)),round(custROS,2),round(champROS,2)
