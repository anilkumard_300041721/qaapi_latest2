import datetime
import pickle

resetCumulative = 0
if resetCumulative == 0:
    with open('../pickle_files/phrasesReport.pkl', 'rb') as phrasesReportF:
        phrasesReport = pickle.load(phrasesReportF)

    with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'rb') as cumulativePhrasesReportDictF:
        cumulativePhrasesReportDict = pickle.load(cumulativePhrasesReportDictF)

    for param,paramDict in cumulativePhrasesReportDict.items():
        for keyPhrase,freqCount in paramDict.items():
            if param != 'statistics' or keyPhrase != 'start_time':
                cumulativePhrasesReportDict[param][keyPhrase] += phrasesReport[param][keyPhrase]

    with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'wb') as f:
        pickle.dump(cumulativePhrasesReportDict, f, pickle.HIGHEST_PROTOCOL)

phrasesReportDict = {}
phrasesReportDict['statistics'] = {'no_of_calls':0, 'repetition': 0, 'over_apology': 0, 'favourable': 0, 'unfavourable': 0, 'opening': 0,'closing': 0, 'ztp': 0, 'hold_time': 0,'start_time : ': str(datetime.datetime.now())}
with open('../pickle_files/champ_check_param_ident_dict.pkl', 'rb') as champ_check_param_ident_dict_file:
    champ_check_param_ident_dict = pickle.load(champ_check_param_ident_dict_file)

with open('../pickle_files/cust_check_param_ident_dict.pkl', 'rb') as cust_check_param_ident_dict_file:
    cust_check_param_ident_dict = pickle.load(cust_check_param_ident_dict_file)

for keyPhrase,param in champ_check_param_ident_dict.items():
    if param not in phrasesReportDict:
        phrasesReportDict[param] = {}
    if keyPhrase not in phrasesReportDict[param]:
        phrasesReportDict[param][keyPhrase] = 0

for keyPhrase,param in cust_check_param_ident_dict.items():
    if param not in phrasesReportDict:
        phrasesReportDict[param] = {}
    if keyPhrase not in phrasesReportDict[param]:
        phrasesReportDict[param][keyPhrase] = 0
with open('../pickle_files/phrasesReport.pkl', 'wb') as f:
    pickle.dump(phrasesReportDict, f, pickle.HIGHEST_PROTOCOL)

if resetCumulative == 1:
    cumulativePhrasesReportDict = phrasesReportDict
    with open('../pickle_files/cumulativePhrasesReportDict.pkl', 'wb') as f:
        pickle.dump(cumulativePhrasesReportDict, f, pickle.HIGHEST_PROTOCOL)
