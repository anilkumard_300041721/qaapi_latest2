#!/usr/bin/env bash

python -m spacy download en_core_web_sm-2.0.0 --direct
python -m spacy download en

python -c "import nltk; nltk.download('stopwords')"