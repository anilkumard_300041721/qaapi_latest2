from nlp_utils import *
from qa_config import *
from time_dependant_params import getTimeDependantParams
import html2text
import json
import pickle
# from upload_phrases_report import uploadAndResetPhrasesReport
import datetime
import os


# A custom stoplist
STOPLIST = set(stopwords.words('english') + list(ENGLISH_STOP_WORDS))
# List of symbols we don't care about
SYMBOLS = []
parser = spacy.load('en')


def get_root(path):
    root = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(root, path)


# loading content objects from qa google spreadsheet
with open(get_root('../pickle_files/champ_check_param_ident_dict.pkl'), 'rb') as champ_check_param_ident_dict_file:
    champ_check_param_ident_dict = pickle.load(champ_check_param_ident_dict_file)

# repetition_champion, repetition_customer, over_apology, opening, closing, ztp
with open(get_root('../pickle_files/champ_check_tokens_lengthWise_dict.pkl'), 'rb') as champ_check_tokens_lengthWise_dict_file:
    champ_check_tokens_lengthWise_dict = pickle.load(champ_check_tokens_lengthWise_dict_file)

# print("## ",champ_check_param_ident_dict['why dont you understand'])
# print("## ",champ_check_tokens_lengthWise_dict[4])

with open(get_root('../pickle_files/cust_check_param_ident_dict.pkl'), 'rb') as cust_check_param_ident_dict_file:
    cust_check_param_ident_dict = pickle.load(cust_check_param_ident_dict_file)

# repetition_champion, repetition_customer, over_apology, opening, closing, ztp

with open(get_root('../pickle_files/cust_check_tokens_lengthWise_dict.pkl'), 'rb') as cust_check_tokens_lengthWise_dict_file:
    cust_check_tokens_lengthWise_dict = pickle.load(cust_check_tokens_lengthWise_dict_file)

with open(get_root('../pickle_files/time_thresholds.pkl'), 'rb') as time_thresholds_file:
    time_thresholds = pickle.load(time_thresholds_file)

with open(get_root('../pickle_files/call_weights.pkl'), 'rb') as call_weights_file:
    call_weights_dict = pickle.load(call_weights_file)
customer_repetition_keywords_lengthWise = {}


def qa_score(champion_text,customer_text, hold_time, talk_time, per_word_info_cust,per_word_info_champ,merged_info):
    # if len(str(champion_text)) < 30 or len(str(customer_text)) < 30:
    #    return None,None,-1,-1,None,None,None,None,None,None
    with open(get_root('../pickle_files/phrasesReport.pkl'), 'rb') as phrasesReportF:
        phrasesReportDict = pickle.load(phrasesReportF)
    phrasesReportDict['statistics']['no_of_calls'] += 1
    champion_text_tokens = champion_text.lower().split()
    #print(champion_text_tokens)
    customer_text_tokens = customer_text.lower().split()
    if (len(champion_text_tokens) != len(per_word_info_champ)) or (len(customer_text_tokens) != len(per_word_info_cust)):
        print("word info length and no of words not matching .....")
    params_list = ["repetition_champion", "repetition_customer", "over_apology", "favourable", "unfavourable", "opening", "closing", "ztp", "before_hold", "after_hold", "ZTP_CustomerCheck", "CouponConditions", "Jargons", "champ_voice_not_audible"]
    flagged_tokens = {}
    score_real = {}
    isCouponRelated = 0
    for param in params_list:
        score_real[param] = 0
        flagged_tokens[param] = ""
    text_onegrams = ngrams(champion_text_tokens, 1)
    if 'coupon' in text_onegrams:
        isCouponRelated = 1
    for ngram_len in champ_check_tokens_lengthWise_dict.keys():
        cur_ngram_keys = champ_check_tokens_lengthWise_dict[ngram_len]
        text_ngrams = ngrams(champion_text_tokens, ngram_len)
        for ngramIdx,cur_text_ngram in enumerate(text_ngrams):
            if cur_text_ngram in cur_ngram_keys:
                parameter = champ_check_param_ident_dict[cur_text_ngram]
                if parameter not in phrasesReportDict:
                    phrasesReportDict[parameter] = {}
                if cur_text_ngram not in phrasesReportDict[parameter]:
                    phrasesReportDict[parameter][cur_text_ngram] = 0
                phrasesReportDict[parameter][cur_text_ngram] += 1
                #print("parameter : ",cur_text_ngram,parameter)
                cur_text_ngram_timestamp = per_word_info_champ[ngramIdx][1]
                flagged_tokens[parameter] = flagged_tokens[parameter] + ' ## ' + cur_text_ngram + '_' + str(datetime.timedelta(seconds=cur_text_ngram_timestamp))[0:7]
                score_real[parameter] += 1
    for ngram_len in cust_check_tokens_lengthWise_dict.keys():
        cur_ngram_keys = cust_check_tokens_lengthWise_dict[ngram_len]
        text_ngrams = ngrams(customer_text_tokens, ngram_len)
        for ngramIdx,cur_text_ngram in enumerate(text_ngrams):
            if cur_text_ngram in cur_ngram_keys:
                parameter = cust_check_param_ident_dict[cur_text_ngram]
                if parameter not in phrasesReportDict:
                    phrasesReportDict[parameter] = {}
                if cur_text_ngram not in phrasesReportDict[parameter]:
                    phrasesReportDict[parameter][cur_text_ngram] = 0
                phrasesReportDict[parameter][cur_text_ngram] += 1
                # print("parameter : ",cur_text_ngram,parameter)
                cur_text_ngram_timestamp = per_word_info_cust[ngramIdx][1]
                flagged_tokens[parameter] = flagged_tokens[parameter] + ' ## ' + cur_text_ngram + '_' + str(datetime.timedelta(seconds=cur_text_ngram_timestamp))[0:7]
                score_real[parameter] += 1

    # converting score to binary flag
    score_binary = score_real.copy()
    # print(score_real)
    score_binary['repetition'] = score_binary['repetition_champion'] + score_binary['repetition_customer']
    score_binary.pop('repetition_champion')
    score_binary.pop('repetition_customer')
    for key in score_binary.keys():
        if key == 'ztp':
            if score_binary[key] > 0:
                phrasesReportDict['statistics']['ztp'] += 1
                score_binary[key] = 1
        elif key == 'opening' or key == 'closing':
            if score_binary[key] == 0:
                phrasesReportDict['statistics'][key] += 1
                score_binary[key] = 1
            else:
                score_binary[key] = 0
        elif key == 'over_apology':
            if score_binary[key] > 2:
                score_binary[key] = 1
                phrasesReportDict['statistics'][key] += 1
            else:
                score_binary[key] = 0
        elif key == 'favourable':
            if score_binary[key] > 0:
                score_binary[key] = 0
            else:
                score_binary[key] = 1
                phrasesReportDict['statistics'][key] += 1
        elif key == 'unfavourable':
            if score_binary[key] > 0:
                score_binary[key] = 1
                phrasesReportDict['statistics'][key] += 1
            else:
                score_binary[key] = 0
        elif key == 'repetition':
            if score_binary['repetition'] > 2:
                score_binary['repetition'] = 1
                phrasesReportDict['statistics'][key] += 1
            else:
                score_binary['repetition'] = 0
        elif key == 'Jargons':
            if score_binary[key] > 0:
                score_binary[key] = 1
                phrasesReportDict['statistics'][key] += 1
            else:
                score_binary[key] = 0
        elif key == 'ZTP_CustomerCheck':
            if score_binary[key] > 0:
                phrasesReportDict['statistics'][key] += 1
                score_binary[key] = 1
            else:
                score_binary[key] = 0
        elif key == 'champ_voice_not_audible':
            if score_binary[key] > 0:
                phrasesReportDict['statistics'][key] += 1
                score_binary[key] = 1
            else:
                score_binary[key] = 0
        elif key == 'CouponConditions':
            if isCouponRelated == 1:
                phrasesReportDict['statistics']['nofCouponCalls'] += 1
                if score_binary[key] == 0:
                    score_binary[key] = 1
                    phrasesReportDict['statistics'][key] += 1
                else:
                    score_binary[key] = 0
            else:
                score_binary[key] = 0

    total_call_duration_aht = talk_time + hold_time
    if total_call_duration_aht == 0:
        return None,None,None,-1,None,None,None,None,None,None
    hold_by_aht = float(hold_time)/total_call_duration_aht

    # checking for on-call performance parameters
    perf_para = {'hold_time': 0, 'talk_time': 0, 'dead_air': 0}

    hold_by_aht_threshold = time_thresholds['hold_by_aht_threshold']
    if hold_by_aht >= float(hold_by_aht_threshold)/100:
        perf_para['hold_time'] = 1
        phrasesReportDict['statistics']['hold_time'] += 1
    score_binary_inv = {key: int(not (score_binary[key])) for key in score_binary}
    perf_para_inv = {key: int(not (perf_para[key])) for key in perf_para}

    # Time Dependant parameters
    interruptsTimestamps, maxDeadAir, deadAirTimeStamp, custROS, champROS = getTimeDependantParams(customer_text,champion_text,per_word_info_cust,per_word_info_champ,merged_info)
    score = 0
    #print(score_binary_inv)
    score_wo_hold = score_binary_inv['repetition']+score_binary_inv['over_apology']+score_binary_inv['opening']+score_binary_inv['closing']+score_binary_inv['favourable']+score_binary_inv['unfavourable']+score_binary_inv['champ_voice_not_audible']+score_binary_inv['CouponConditions']+score_binary_inv['Jargons']+score_binary_inv['ZTP_CustomerCheck']
    #print("score_wo_hold : ",score_wo_hold)
    # print("\n call_weights_dict : \n", call_weights_dict)
    #print("\n score_binary : \n ", score_binary)
    #print("\n flagged_tokens : \n",flagged_tokens)
    # print("\n score : \n", score_wo_hold,score_with_hold)
    #print("\n score_real : \n ",score_real)
    # print("\n perf_para : \n ", perf_para)
    #return score_binary, score_real, perf_para, score, flagged_tokens
    # with open('../pickle_files/phrasesReport.pkl', 'wb') as f:
    #     pickle.dump(phrasesReportDict, f, pickle.HIGHEST_PROTOCOL)
    # print("call number :",phrasesReportDict['statistics']['no_of_calls'])
    # if phrasesReportDict['statistics']['no_of_calls'] == 16000:
    #     uploadAndResetPhrasesReport(phrasesReportDict)
    #print(flagged_tokens)
    #print(score_binary)
    #print(score_real)
    #print(isCouponRelated)
    return score_binary, score_real, perf_para, score_wo_hold, flagged_tokens,interruptsTimestamps, maxDeadAir, deadAirTimeStamp, custROS, champROS

from collections import OrderedDict
def get_empty_record():
    record = OrderedDict()
    record["emp_name"] = ""
    record["interaction_id"] = ""
    record["incident_id"] = "0000"
    record["phone"] = ""
    record["customer_call_content"] = ""
    record["champion_call_content"] = ""
    record["interaction_timestamp"] = ""
    record["actual_dispostion_l1"] = ""
    record["actual_dispostion_l2"] = ""
    record["actual_dispostion_l3"] = ""
    record["score"] = 0
    record["repetition"] = False
    record["repetition_tokens_champion"] = ""
    record["repetition_tokens_customer"] = ""
    record["over_apology"] = False
    record["over_apology_tokens"] = ""
    record["opening"] = False
    record["opening_tokens"] = ""
    record["closing"] = False
    record["closing_tokens"] = ""
    record["ztp"] = False
    record["ztp_tokens"] = ""
    record["favourable"] = False
    record["favourable_tokens"] = ""
    record["unfavourable"] = False
    record["unfavourable_tokens"] = ""
    record["hold_time"] = False
    record["hold_time_val"] = 0
    record["hold_permission_before"] = False
    record["hold_permission_tokens_before"] = ""
    record["hold_permission_after"] = False
    record["hold_permission_tokens_after"] = ""
    record["ZTP_CustomerCheck"] = False
    record["ZTP_CustomerCheck_tokens"] = ""
    record["CouponConditions"] = False
    record["CouponConditions_tokens"] = ""
    record["Jargons"] = False
    record["Jargons_tokens"] = ""
    record["champ_voice_not_audible"] = False
    record["champ_voice_not_audible_tokens"] = ""
    record["dead_air"] = False
    record["dead_air_val"] = ""
    record["deadAirTimeStamp"] = "invalid"
    record["talk_time_val"] = 0
    record["custROS"] = "invalid"
    record["champROS"] = "invalid"
    record["interruptsTimestamps"] = ""
    record["merged_text"] = ""
    return record

def get_qa_record(line):
    message_interaction = line
    content = ""
    record = get_empty_record()
    record["incident_id"] = "0000"
    # record["order_id"] = message_interaction["order_id"]
    if "crt_obj_id" in message_interaction:
        # Call
        champion_text = message_interaction["champion_text"]
        customer_text = message_interaction["customer_text"]
        record["customer_call_content"] = customer_text
        record["champion_call_content"] = champion_text
        record["emp_name"] = message_interaction["champion_name"]
        record["interaction_id"] = message_interaction["crt_obj_id"]
        #record["source"] = "Call"
        record["interaction_timestamp"] = message_interaction["time_sent"]
        #print(record["interaction_timestamp"])
        record["hold_time_val"] = int(message_interaction.get("hold_time", 0))
        record["talk_time_val"] = int(message_interaction.get("talk_time", 0))
        #record["dead_air_val"] = int(float(message_interaction.get("dead_air", 0)))
        #record['AHT'] = record["talk_time_val"] + record["hold_time_val"]
        record["merged_text"] = message_interaction.get("merged_info", "")
        record["phone"] = message_interaction.get("phone", "")
    else:
        # Email
        text_maker = html2text.HTML2Text()
        text_maker.ignore_links = True
        text_maker.bypass_tables = False
        content = text_maker.handle(message_interaction["email_content"])
        record["interaction_id"] = message_interaction["incident_thread_id"]
        record["emp_name"] = message_interaction["author"]
        #record["email_content"] = content
        #record["source"] = "Mail"
        record["interaction_timestamp"] = message_interaction["timestamp"].strip('\'')
        record["hold_time_val"] = int(message_interaction.get("hold_time", 0))
        record["talk_time_val"] = int(message_interaction.get("talk_time", 0))
        #record["champion_id"] = message_interaction.get("champion_id", "")

    #record["user_email"] = record.get("user_email", "")
    #considering only calls for now.
    import ast
    per_word_info_cust = message_interaction["per_word_info_cust"]
    per_word_info_champ = message_interaction["per_word_info_champ"]
    try:
        per_word_info_cust = ast.literal_eval(per_word_info_cust)
        per_word_info_champ = ast.literal_eval(per_word_info_champ)
    except:
        print("Invalid input ...")
        return None
    merged_info = record['merged_text']
    merged_info = ast.literal_eval(merged_info)
    #print("New params : ",interruptsTimestamps, maxDeadAir, deadAirTimeStamp, custROS, champROS)
    if "crt_obj_id" in message_interaction:
        #qa_score(champion_text, customer_text, record["hold_time_val"], record["talk_time_val"], record["dead_air_val"],record["source"] == "Mail", True)
        score_binary, score_real, perf_para, score, flagged_tokens,interruptsTimestamps, maxDeadAir, deadAirTimeStamp, custROS, champROS = qa_score(champion_text,customer_text, record["hold_time_val"], record["talk_time_val"],per_word_info_cust,per_word_info_champ,merged_info)
        # if champROS < 0 or custROS < 0:
        #     with open("wrongROS.txt", 'a') as wros:
        #         wros.write(line)
        if score == -1:
            return None
        if maxDeadAir == None:
            print("Nof words not matching")
        else:
            record["interruptsTimestamps"] = str(interruptsTimestamps)
            if maxDeadAir >= 8.0:
                record['dead_air'] = bool(1)
            record["dead_air_val"] = str(maxDeadAir)
            record["deadAirTimeStamp"] = str(deadAirTimeStamp)
            record["custROS"] = str(custROS)
            record["champROS"] = str(champROS)
        record["repetition"] = bool(score_binary["repetition"])
        record["over_apology"] = bool(score_binary["over_apology"])
        record["closing"] = bool(score_binary["closing"])
        record["opening"] = bool(score_binary["opening"])
        record["favourable"] = bool(score_binary["favourable"])
        record["unfavourable"] = bool(score_binary["unfavourable"])
        record["ztp"] = bool(score_binary["ztp"])
        record["ZTP_CustomerCheck"] = bool(score_binary["ZTP_CustomerCheck"])
        record["CouponConditions"] = bool(score_binary["CouponConditions"])
        record["Jargons"] = bool(score_binary["Jargons"])
        record["champ_voice_not_audible"] = bool(score_binary["champ_voice_not_audible"])
        record['hold_time'] = bool(perf_para['hold_time'])
        # if perf_para['hold_time'] == 1:
        #     with open("holdInput.txt",'a') as wHold:
        #         wHold.write(line)
        # "ZTP_CustomerCheck", "CouponConditions", "Jargons", "champ_voice_not_audible"
        if record["hold_time_val"] > 0:
            if score_real['before_hold'] == 0:
                record["hold_permission_before"] = bool(1)
            else:
                score += 1
            if score_real['after_hold'] == 0:
                record["hold_permission_after"] = bool(1)
            else:
                score += 1
        else:
            score += 1
        record["hold_permission_tokens_before"] = flagged_tokens['before_hold']
        record["hold_permission_tokens_after"] = flagged_tokens['after_hold']
        record["repetition_tokens_champion"] = flagged_tokens['repetition_champion']
        record["repetition_tokens_customer"] = flagged_tokens['repetition_customer']
        record["over_apology_tokens"] = flagged_tokens['over_apology']
        record["opening_tokens"] = flagged_tokens['opening']
        record["closing_tokens"] = flagged_tokens['closing']
        record["ztp_tokens"] = flagged_tokens['ztp']
        #"ZTP_CustomerCheck", "CouponConditions", "Jargons", "champ_voice_not_audible"
        record["favourable_tokens"] = flagged_tokens['favourable']
        record["unfavourable_tokens"] = flagged_tokens['unfavourable']
        record["ZTP_CustomerCheck_tokens"] = flagged_tokens['ZTP_CustomerCheck']
        record["CouponConditions_tokens"] = flagged_tokens['CouponConditions']
        record["Jargons_tokens"] = flagged_tokens['Jargons']
        record["champ_voice_not_audible_tokens"] = flagged_tokens['champ_voice_not_audible']
        record["score"] = int(score)

        record["actual_dispostion_l1"] = message_interaction.get("actual_dispostion_l1", "")
        record["actual_dispostion_l2"] = message_interaction.get("actual_dispostion_l2", "")
        record["actual_dispostion_l3"] = message_interaction.get("actual_dispostion_l3", "")
        #print(record['hold_time'],record["hold_permission"],record["hold_permission_tokens"])
        if record["emp_name"] == "Customer":
            m = 1
        else:
            return record
        return None
import ast
with open('input_samplt.txt','r') as ff:
    line3 = ff.readlines()[0]
line3 = ast.literal_eval(line3)
print(line3)
#print(get_qa_record(line3))
