FROM ubuntu:18.04
MAINTAINER remidi "yashwanth.remidi@myntra.com"

RUN apt-get update -y && apt-get install -y python3-pip build-essential

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

RUN python3 -m spacy download en_core_web_sm-2.0.0 --direct \
&& python3 -m spacy download en \
&& python3 -c "import nltk; nltk.download('stopwords')"

RUN pip3 install gunicorn==19.9.0

COPY . /src
WORKDIR /src/qa_core

CMD ["gunicorn", "-b", "0.0.0.0:2777", "--workers=2", "wsgi:app"]
