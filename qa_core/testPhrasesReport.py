import json
import pickle
from pandas import ExcelWriter
from datetime import datetime, timedelta
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from apiclient.http import MediaIoBaseDownload
from qa_config import service_account_secret_path

#phrasesReportDict = {}
#phrasesReportDict['statistics'] = {'no_of_calls':0, 'repetition': 0, 'over_apology': 0, 'favourable': 0, 'unfavourable': 0, 'opening': 0,'closing': 0, 'ztp': 0, 'hold_time': 0,'start_time': str(datetime.now())}
#with open('../pickle_files/champ_check_param_ident_dict.pkl', 'rb') as champ_check_param_ident_dict_file:
#    champ_check_param_ident_dict = pickle.load(champ_check_param_ident_dict_file)

#with open('../pickle_files/cust_check_param_ident_dict.pkl', 'rb') as cust_check_param_ident_dict_file:
#    cust_check_param_ident_dict = pickle.load(cust_check_param_ident_dict_file)

#for keyPhrase,param in champ_check_param_ident_dict.items():
#    if param not in phrasesReportDict:
#        phrasesReportDict[param] = {}
#    if keyPhrase not in phrasesReportDict[param]:
##        phrasesReportDict[param][keyPhrase] = 0

#for keyPhrase,param in cust_check_param_ident_dict.items():
#    if param not in phrasesReportDict:
#        phrasesReportDict[param] = {}
#    if keyPhrase not in phrasesReportDict[param]:
#        phrasesReportDict[param][keyPhrase] = 0
#with open('../pickle_files/phrasesReport.pkl', 'wb') as f:
#    pickle.dump(phrasesReportDict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/phrasesReport.pkl', 'rb') as champ_check_param_ident_dict_file:
    phrasesReportDict = pickle.load(champ_check_param_ident_dict_file)
print(phrasesReportDict['statistics'])
