import argparse
import pandas as pd
from connector import SentientDB
from qa_function import get_qa_record
import datetime
import json
import time
import os
from utils import get_logger
from utils import send_mail

ti = time.time()

qalogger = get_logger('sentient.qa')


parser = argparse.ArgumentParser(description='QA direct mysql consumer')
parser.add_argument('--daily', help='daily run', action='store_true')
parser.add_argument('--from_ts', help='from date range for the qa run', type=str, default=None)
parser.add_argument('--to_ts', help='to timestamp from date range for qa run', type=str, default=None)
parser.add_argument('--debug', help='debug the code', action='store_true')
parser.add_argument('--outdir', help='out directory for the qa run', type=str, default='/myntra/sentient/qa')
args = parser.parse_args()

if args.daily:
    args.to_ts = datetime.datetime.now().strftime('%Y-%m-%d 00:00:00')
    args.from_ts = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d 00:00:00')

if args.from_ts is None or args.to_ts is None:
    raise ValueError('datetime error')

if not os.path.exists(args.outdir):
    os.makedirs(args.outdir)

# if args.debug:
#     with open('sample.json', '+r') as f:
#         j = json.load(f)
#     jsons = [j]
# else:
#     db = SentientDB()
#     jsons = db.sentient_select(args.from_ts, args.to_ts)
#     qalogger.info('\n total calls in range ({}) to ({}) ==> ({})'.format(args.from_ts, args.to_ts, len(jsons)))
#     db.close()

db = SentientDB()
jsons = db.sentient_select(args.from_ts, args.to_ts)
qalogger.info('\n total calls in range ({}) to ({}) ==> ({})'.format(args.from_ts, args.to_ts, len(jsons)))
db.close()

records = list()
processed = 0
for data in jsons:
    try:
        record = get_qa_record(data)
        records.append(record)
        processed += 1
    except Exception as e:
        qalogger.error('error in processing ==>  {} ==> {}'.format(data.get('crt_obj_id', 'No crt_obj_id'), e))

col_order = ('emp_name', 'customer_call_content', 'champion_call_content', 'merged_text', 'interruptsTimestamps',
             'champROS', 'custROS', 'repetition', 'repetition_tokens_champion', 'repetition_tokens_customer',
             'over_apology', 'over_apology_tokens', 'closing', 'closing_tokens', 'opening', 'opening_tokens',
             'favourable', 'favourable_tokens', 'unfavourable', 'unfavourable_tokens', 'ztp', 'ztp_tokens',
             'champ_voice_not_audible', 'champ_voice_not_audible_tokens', 'Jargons', 'Jargons_tokens',
             'CouponConditions', 'CouponConditions_tokens', 'ZTP_CustomerCheck', 'ZTP_CustomerCheck_tokens',
             'interaction_timestamp', 'hold_time', 'hold_time_val', 'hold_permission_before',
             'hold_permission_tokens_before', 'hold_permission_after', 'hold_permission_tokens_after',
             'talk_time_val', 'dead_air', 'dead_air_val', 'deadAirTimeStamp', 'score', 'actual_dispostion_l1',
             'actual_dispostion_l2', 'actual_dispostion_l3', 'phone', 'incident_id', 'interaction_id')

qadf = pd.DataFrame(records)
qadf = qadf.loc[:, col_order]
qadf.to_csv(os.path.join(args.outdir, 'sentientqa_{}.csv.gz'.format(args.from_ts.split(' ')[0])),
            index=False,
            compression='gzip')

if args.debug:
    mailers = ['yashwanth.remidi@myntra.com', 'anil.kumar@myntra.com']
    mailers = [', '.join(mailers)]
else:
    mailers = ['yashwanth.remidi@myntra.com', 'anil.kumar@myntra.com', 'rakesh.s@myntra.com',
               'radhika.mittal@myntra.com', 'aparna.sastry@myntra.com', 'priya.rajagopal@myntra.com',
               'quality-cc@myntra.com', 'manish.kumar@myntra.com']

    mailers = [', '.join(mailers)]

for mailer in mailers:
    try:
        send_mail(mailer,
                  subject='QA Production Data Mail',
                  body=('Guys,'
                        '\nPlease find the attached QA production data file.'
                        '\n\n'
                        '\nData received from the liv.ai between {from_ts} to {to_ts}: {liv}'
                        '\nData processed by the QA V 1.5 production: {processed}'
                        '\n\nThanks,'
                        '\nYashwanth Remidi,'
                        '\n Data Scientist-I, Myntra Designs.\n\n').format(liv=len(jsons),
                                                                           processed=processed,
                                                                           from_ts=args.from_ts,
                                                                           to_ts=args.to_ts),
                  filename=os.path.join(args.outdir, 'sentientqa_{}.csv.gz'.format(args.from_ts.split(' ')[0])),
                  attachment_name='sentientqa_{}.csv.gz'.format(args.from_ts.split(' ')[0]))
    except Exception as e:
        qalogger.error('error while sending mail to ({}); {}'.format(mailer, e))


qalogger.info('time taken: ({}) sec'.format(time.time() - ti))





