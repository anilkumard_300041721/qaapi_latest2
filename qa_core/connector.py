import mysql.connector
from utils import convertor
import datetime


class SentientDB(object):
    def __init__(self):
        self.counter = {}
        self.convertor = convertor

        self.connection = mysql.connector.connect(user='root', password='sentient123',
                                                  host='127.0.0.1',
                                                  database='sentient',
                                                  charset='utf8')

        self.cursor = self.connection.cursor()

    def close(self):
        self.connection.commit()

        self.cursor.close()
        self.connection.close()

    def sentient_select(self, from_ts, to_ts):

        query = 'select ' \
                'distinct cm.crt_obj_id as crt_obj_id, ' \
                'cm.hold_time as hold_time, ' \
                'cm.talk_time as talk_time, ' \
                'cm.phone as phone, ' \
                'ct.champion_text as champion_text, ' \
                'ct.customer_text as customer_text, ' \
                'ct.merged_info as merged_info, ' \
                'ct.per_word_info_cust as per_word_info_cust, ' \
                'ct.per_word_info_champ as per_word_info_champ, ' \
                'cm.call_end_time as timr_sent, ' \
                'ch.champion_name as champion_name ' \
                'from call_metadata cm ' \
                'inner join call_transcripts ct on cm.crt_obj_id = ct.crt_obj_id ' \
                'inner join sentient_counter sc on cm.crt_obj_id = sc.crt_obj_id ' \
                'left outer join champions ch on cm.crt_obj_id = ch.crt_obj_id ' \
                'where sc.counter > 2 ' \
                'and sc.modify_ts between \'{from_ts}\' and \'{to_ts}\'; '.format(from_ts=from_ts, to_ts=to_ts)

        self.cursor.execute(query)

        select = list()

        for (crt_obj_id,
             hold_time,
             talk_time,
             phone,
             champion_text,
             customer_text,
             merged_info,
             per_word_info_cust,
             per_word_info_champ,
             time_sent,
             champion_name) in self.cursor:

            data = dict(crt_obj_id=crt_obj_id,
                        hold_time=hold_time,
                        talk_time=talk_time,
                        phone=phone,
                        champion_text=champion_text,
                        customer_text=customer_text,
                        merged_info=merged_info,
                        per_word_info_cust=per_word_info_cust,
                        per_word_info_champ=per_word_info_champ,
                        time_sent=time_sent,
                        champion_name=champion_name)

            select.append(data)

        return select

    def destroy(self):
        tables = ['call_metadata', 'call_transcripts', 'sentient_counter']
        for table in tables:
            query = 'drop table if exists `{}`;'.format(table)
            self.execute(query)


    def create(self):
        create_call_table = ('create table if not exists `call_metadata` ('
                             'crt_obj_id text,'
                             'hangup_cause text,'
                             'hangup_first text,'
                             'hangup_on_hold text,'
                             'hold_time text,'
                             'is_outbound text,'
                             'ivr_time text, '
                             'num_attempts text,'
                             'ringing_time text, '
                             'setup_time text,'
                             'talk_time text,'
                             'call_end_time text,'
                             'hangup_cause_description text,'
                             'phone text,'
                             'recording_file_url text);')

        create_counter_table = 'create table if not exists `sentient_counter` (' \
                               'crt_obj_id varchar(255), ' \
                               'counter smallint,' \
                               'modify_ts timestamp default current_timestamp on update current_timestamp,' \
                               'primary key (crt_obj_id));'

        create_call_transcripts_table = ('create table if not exists `call_transcripts` ('
                                         'crt_obj_id text,'
                                         'champion_text blob,'
                                         'customer_text blob,'
                                         'merged_info blob,'
                                         'per_word_info_cust text,'
                                         'per_word_info_champ text,'
                                         'time_sent text);')

        self.execute(create_call_table)
        self.execute(create_call_transcripts_table)
        self.execute(create_counter_table)


    def execute(self, query: str):
        status = 0
        try:
            self.cursor.execute(query)
            self.connection.commit()
            status = 1
        except ValueError as e:
            print('ValueError in the sentient.sql: ({})'.format(str(e)))
        return status

    def insert(self, table: str, json: dict):
        crt_obj_id = json['crt_obj_id']

        cols = ', '.join(json.keys())

        row = self.convertor.get(table)(json)
        row = self.row_joiner(row)
        query = 'insert into `{}` ({}) values ({});'.format(table, cols, row)
        status = self.execute(query)
        if status:
            self.update_counter(crt_obj_id)

    def update_counter(self, uuid: str):
        uuid = '\'{}\''.format(uuid)
        query = 'insert into sentient_counter (crt_obj_id, counter) values ({}, 1) ' \
                'on duplicate key update counter = counter + 1;'.format(uuid)

        self.execute(query)

    @staticmethod
    def row_joiner(row: list):
        return ', '.join(['\'{}\''.format(x) for x in row])


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--time', default=60, type=int)
    args = parser.parse_args()

    from_ts = (datetime.datetime.now() - datetime.timedelta(seconds=args.time)).strftime('%Y-%m-%d %H:%M:%S')
    to_ts = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    print(from_ts)
    print(to_ts)

    db = SentientDB()
    r = db.sentient_select(from_ts, to_ts)

    print(len(r))
    db.close()





