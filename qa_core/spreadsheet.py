import nlp_utils
import pickle
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from qa_config import token_obj_path, client_secret_path
from nlp_utils import tokenizeText

import json

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name(client_secret_path,scope)

file = gspread.authorize(creds) # authenticate with Google
sentient_qa = file.open("Sentient_qa_v2_sheet") # open sheet

# load text from qa google spreadsheet
PhrasesKeywords_Repetition_Cham = sentient_qa.worksheet('PhrasesKeywords_Repetition_Cham')
PhrasesKeywords_Repetition_Cust = sentient_qa.worksheet('PhrasesKeywords_Repetition_Cust')
ApologyPhrasesKeywords = sentient_qa.worksheet('ApologyPhrasesKeywords')
Favourable_Keywords = sentient_qa.worksheet('Favourable_Keywords')
Unfavourable_Keywords = sentient_qa.worksheet('Unfavourable_Keywords')
time_thresholds = sentient_qa.worksheet('time_thresholds')
PhrasesKeywords_Opening = sentient_qa.worksheet('PhrasesKeywords_Opening')
PhrasesKeywords_Closing = sentient_qa.worksheet('PhrasesKeywords_Closing')
before_hold = sentient_qa.worksheet('Before_Hold')
after_hold = sentient_qa.worksheet('After_Hold')
ZTP = sentient_qa.worksheet('ZTP')
ZTP_CustomerCheck = sentient_qa.worksheet('ZTP_CustomerCheck')
CouponConditions = sentient_qa.worksheet('CouponConditions')
Jargons = sentient_qa.worksheet('Jargons')
champ_voice_not_audible = sentient_qa.worksheet('champ_voice_not_audible')
call_weights_file = sentient_qa.worksheet('call_weights')

#Hold_percent_Of_Call = nlp_utils.engtokenize(Hold_percent_Of_Call)
#print(Hold_percent_Of_Call.get_all_values())
#print(ApologyPhrasesKeywords.get_all_values())
champ_check_param_ident_dict = {}
cust_check_param_ident_dict = {} #{'i am sorry' : 'apology'}
champ_check_tokens_lengthWise_dict = {} #{1:[hello,hi], 2:["good morning","good afternoon"]}
cust_check_tokens_lengthWise_dict = {}

for row in PhrasesKeywords_Repetition_Cham.get_all_values():
    curPhrase = row[0].lower()
    #print(curPhrase)
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-repeat-champion keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "repetition_champion"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in PhrasesKeywords_Repetition_Cust.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in cust_check_param_ident_dict:
        print(curPhrase,"-repeat-cust keyword already there in some other parameter")
    else:
        cust_check_param_ident_dict[curPhrase] = "repetition_customer"
    if curPhraseLen not in cust_check_tokens_lengthWise_dict:
        cust_check_tokens_lengthWise_dict[curPhraseLen] = []
    cust_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in ApologyPhrasesKeywords.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-apology keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "over_apology"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in Favourable_Keywords.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-favourable keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "favourable"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

    if curPhrase in cust_check_param_ident_dict:
        print(curPhrase,"-favourable keyword already there in some other parameter")
    else:
        cust_check_param_ident_dict[curPhrase] = "favourable"
    if curPhraseLen not in cust_check_tokens_lengthWise_dict:
        cust_check_tokens_lengthWise_dict[curPhraseLen] = []
    cust_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in Unfavourable_Keywords.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-Unfavourable keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "unfavourable"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

    if curPhrase in cust_check_param_ident_dict:
        print(curPhrase,"-Unfavourable keyword already there in some other parameter")
    else:
        cust_check_param_ident_dict[curPhrase] = "unfavourable"
    if curPhraseLen not in cust_check_tokens_lengthWise_dict:
        cust_check_tokens_lengthWise_dict[curPhraseLen] = []
    cust_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in PhrasesKeywords_Opening.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-opening keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "opening"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in PhrasesKeywords_Closing.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-closing keyword already there in some other parameter",champ_check_param_ident_dict[curPhrase])
    else:
        champ_check_param_ident_dict[curPhrase] = "closing"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in before_hold.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-before-hold keyword already there in some other parameter",champ_check_param_ident_dict[curPhrase])
    else:
        champ_check_param_ident_dict[curPhrase] = "before_hold"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in after_hold.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-after_hold keyword already there in some other parameter",champ_check_param_ident_dict[curPhrase])
    else:
        champ_check_param_ident_dict[curPhrase] = "after_hold"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in ZTP.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-ztp keyword already there in some other parameter",champ_check_param_ident_dict[curPhrase])
    else:
        champ_check_param_ident_dict[curPhrase] = "ztp"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in ZTP_CustomerCheck.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in cust_check_param_ident_dict:
        print(curPhrase,"-ZTP_CustomerCheck keyword already there in some other parameter")
    else:
        cust_check_param_ident_dict[curPhrase] = "ZTP_CustomerCheck"
    if curPhraseLen not in cust_check_tokens_lengthWise_dict:
        cust_check_tokens_lengthWise_dict[curPhraseLen] = []
    cust_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in CouponConditions.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-CouponConditions keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "CouponConditions"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in Jargons.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in champ_check_param_ident_dict:
        print(curPhrase,"-Jargons keyword already there in some other parameter")
    else:
        champ_check_param_ident_dict[curPhrase] = "Jargons"
    if curPhraseLen not in champ_check_tokens_lengthWise_dict:
        champ_check_tokens_lengthWise_dict[curPhraseLen] = []
    champ_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

for row in champ_voice_not_audible.get_all_values():
    curPhrase = row[0].lower()
    #curPhrase = ' '.join(tokenizeText(curPhrase))
    curPhraseLen = len(curPhrase.split())
    if curPhrase in cust_check_param_ident_dict:
        print(curPhrase,"-champ_voice_not_audible keyword already there in some other parameter")
    else:
        cust_check_param_ident_dict[curPhrase] = "champ_voice_not_audible"
    if curPhraseLen not in cust_check_tokens_lengthWise_dict:
        cust_check_tokens_lengthWise_dict[curPhraseLen] = []
    cust_check_tokens_lengthWise_dict[curPhraseLen].append(curPhrase)

time_thresholds_dict = {}
time_thresholds_dict['hold_by_aht_threshold'] = float(time_thresholds.get_all_values()[0][2])

call_weights_dict = {}
for row in call_weights_file.get_all_values():
    call_weights_dict[row[0]] = float(row[1])/100
print(call_weights_dict)
#print("## ",champ_check_param_ident_dict['why dont you understand'])
#print("## ",champ_check_tokens_lengthWise_dict[4])
with open('../pickle_files/champ_check_param_ident_dict.pkl', 'wb') as f:
    pickle.dump(champ_check_param_ident_dict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/champ_check_tokens_lengthWise_dict.pkl', 'wb') as f:
    pickle.dump(champ_check_tokens_lengthWise_dict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/cust_check_param_ident_dict.pkl', 'wb') as f:
    pickle.dump(cust_check_param_ident_dict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/cust_check_tokens_lengthWise_dict.pkl', 'wb') as f:
    pickle.dump(cust_check_tokens_lengthWise_dict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/time_thresholds.pkl', 'wb') as f:
    pickle.dump(time_thresholds_dict, f, pickle.HIGHEST_PROTOCOL)
with open('../pickle_files/call_weights.pkl', 'wb') as f:
    pickle.dump(call_weights_dict, f, pickle.HIGHEST_PROTOCOL)
