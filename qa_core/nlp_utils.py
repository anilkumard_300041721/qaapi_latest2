from bs4 import BeautifulSoup
from textblob import TextBlob
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from nltk.corpus import stopwords
import re
import json
import spacy

parser = spacy.load('en')

# A custom stoplist
STOPLIST = set(stopwords.words('english') + list(ENGLISH_STOP_WORDS))

# List of symbols we don't care about
SYMBOLS = []


# A custom function to clean the text
def cleanText(text):
    # get rid of newlines
    text = text.strip().replace("\n", " ").replace("\r", " ")

    # lowercase
    text = text.lower()

    return text


# A custom function to tokenize the text using spaCy
# and convert to lemmas
def tokenizeText(sample):
    # get the tokens using spaCy
    # print sample
    # print type(sample)
    tokens = parser(sample)
    # tokens = unicode(sample, encoding="utf-8")

    # punctuation removal
    tokens = [tok for tok in tokens if tok.is_punct == False]
    tokens = [tok for tok in tokens if tok.is_digit == False]
    # removing words with length less than 3
    # tokens = [tok for tok in tokens if len(tok)>1]

    # lemmatize(not doing)
    lemmas = []
    for tok in tokens:
        # lemmas.append(tok.lemma_.lower().strip() if tok.lemma_ != "-PRON-" else tok.lower_)
        lemmas.append(tok.lower_)
    tokens = lemmas
    # stoplist the tokens
    ####tokens = [tok for tok in tokens if tok not in STOPLIST]

    # stoplist symbols
    tokens = [tok for tok in tokens if tok not in SYMBOLS]
    # remove large strings of whitespace
    while "" in tokens:
        tokens.remove("")
    while " " in tokens:
        tokens.remove(" ")
    while "\n" in tokens:
        tokens.remove("\n")
    while "\n\n" in tokens:
        tokens.remove("\n\n")
    while "'" in tokens:
        tokens.remove("'")

    return tokens


def removeTags(s):
    # Function to clean HTML tags from string (if present, else return string)
    # Assumes HTML syntax is correct, else will remove all strings with RE: < string >
    # Parameters: s - string
    # Returns: string (HTML tags removed)

    try:
        return BeautifulSoup(s, "lxml").text
    except:
        return re.sub(re.compile('<.*?>'), '', s)


def getSentiment(s):
    # Function to get sentiment of a string using TextBlob library
    # Parameters: s - string
    # Returns: <np.float32> score varying from -1 (negative) to 1 (positive)

    return TextBlob(s).sentiment[0]


# ngram units of input of size n
def ngrams(input, n):
    # input = input.split(' ')
    output = []
    for i in range(len(input) - n + 1):
        g = ' '.join(input[i:i + n])
        output.append(g)
    return output


# tokenize english text from qa google spreadsheet
def engtokenize(sheet):
    tokens = []
    sheet_values = sheet.get_all_values()
    for i in sheet_values:
        tokens.extend(tokenizeText(i[0]))
    return set(tokens)


# tokenize hinglish text from qa google spreadsheet
def hintokenize(sheet):
    tokens = []
    sheet_values = sheet.get_all_values()
    for i in sheet_values:
        tokens.extend(ngrams(i[0], 3))
    return set(tokens)


"""
Custom data cleaning functions
"""


def to_timestamp(val):
    return pd.to_datetime(val[:-6] + ':' + val[-5:], dayfirst=True)


def cleanhtml(raw_html):
    try:
        x = BeautifulSoup(raw_html, "lxml").text
        return x
    except:
        return raw_html


def disp_split_cat(disp, t):
    for i in range(len(disp.split('->'))):
        if i == t:
            return disp.split('->', 3)[i].strip()


def note_interpreter(row):
    if row['Thread Entry Type'] == 'Note':
        if row['Text'].find('Customer Input Fields') > -1:
            return row['Text'].split(':', 1)[1].strip().strip('{').strip('}').split(',')
        else:
            return 'Null'
    else:
        return 'Null'


def total_seconds(val):
    return abs(val.total_seconds())


def jsonparser(val):
    if type(val) == dict:
        try:
            x = val['userNotes']
            return x
        except:
            return val
    else:
        try:
            x = json.loads(val)['userNotes']
            return x
        except:
            return val


def repeatcallers(val):
    try:
        if val > 1.0:
            return 1
        else:
            return 0
    except:
        return 0


def removecontacts(val):
    try:
        val = re.sub('[\w\.-]+@[\w\.-]+', '', val)
        val = re.sub('\\d{10}', '', val)
        return val
    except:
        return val    





