from attrdict import AttrDict


def _call_meta_convert(json: dict):
    data = AttrDict(json)

    return [data.crt_obj_id,
            data.hangup_cause,
            data.hangup_first,
            data.hangup_on_hold,
            data.hold_time,
            data.is_outbound,
            data.ivr_time,
            data.num_attempts,
            data.ringing_time,
            data.setup_time,
            data.talk_time,
            data.call_end_time,
            data.hangup_cause_description,
            data.phone,
            data.recording_file_url]


def _call_transcript_convert(json: dict):
    data = AttrDict(json)

    return [data.crt_obj_id,
            data.champion_text.replace('"', '').replace('\'', '').replace('\\', ''),
            data.customer_text.replace('"', '').replace('\'', '').replace('\\', ''),
            data.merged_info.replace('\'', ''),
            data.per_word_info_cust,
            data.per_word_info_champ,
            data.time_sent]


def _champions(json: dict):
    data = AttrDict(json)

    return [data.crt_obj_id,
            data.champion_name]


convertor = {'call_metadata': _call_meta_convert,
             'call_transcripts': _call_transcript_convert,
             'champions': _champions}
